package fa.training.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

		http.csrf(csrf -> csrf.disable())
				.authorizeHttpRequests(
						request -> request.requestMatchers("/registration/**").permitAll().requestMatchers("/login/**")
								.permitAll().requestMatchers("/user/**").hasAnyRole("USER", "ADMIN")
								.requestMatchers("/admin/**").hasAnyRole("ADMIN").anyRequest().authenticated()

				)
				.formLogin(form -> form.loginPage("/login").loginProcessingUrl("/loginProcess")
						.defaultSuccessUrl("/user/").failureUrl("/login?error").permitAll())

				.logout(logout -> logout.logoutSuccessUrl("/login?logout").permitAll())
				.exceptionHandling(ex -> ex.accessDeniedPage("/access-denied"));

		return http.build();

	}
}
