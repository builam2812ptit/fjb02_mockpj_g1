package fa.training.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InitController {

	 @GetMapping("/login")
	  public String showLoginForm() {
	    return "login";
	  }
}
